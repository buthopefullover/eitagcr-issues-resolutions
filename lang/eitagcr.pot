# Copyright (C) 2021 Cooperativa EITA
# This file is distributed under the same license as the EITA GCR plugin.
msgid ""
msgstr ""
"Project-Id-Version: EITA GCR 0.0.1\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/eitagcr\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2021-06-14T18:55:30-03:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"X-Generator: WP-CLI 2.4.0\n"
"X-Domain: eitagcr\n"

#. Plugin Name of the plugin
msgid "EITA GCR"
msgstr ""

#. Plugin URI of the plugin
msgid "https://gitlab.com/eita/eitagcr"
msgstr ""

#. Description of the plugin
msgid "This plugin offers some enhancements to the Woocommerce e-commerce platform to make it fit for using in Responsible Consumer Groups (GCR). It assumes GCRs are composed by several consumers and several suppliers, and the there is a buying cycle."
msgstr ""

#. Author of the plugin
msgid "Cooperativa EITA"
msgstr ""

#. Author URI of the plugin
msgid "https://eita.coop.br"
msgstr ""

#: includes/class-eitagcr-settings.php:124
#: includes/class-eitagcr-settings.php:514
#: includes/class-eitagcr-settings.php:544
#: includes/class-eitagcr-settings.php:564
#: includes/class-eitagcr-settings.php:586
#: includes/class-eitagcr-settings.php:605
#: includes/class-eitagcr-settings.php:626
#: includes/class-eitagcr.php:144
msgid "Cycle"
msgstr ""

#: includes/class-eitagcr-settings.php:250
msgid "Report generated successfully"
msgstr ""

#: includes/class-eitagcr-settings.php:257
msgid "Cycle created successfully"
msgstr ""

#: includes/class-eitagcr-settings.php:299
#: includes/class-eitagcr-settings.php:300
#: includes/class-eitagcr-settings.php:728
msgid "Consumo Responsável"
msgstr ""

#: includes/class-eitagcr-settings.php:348
msgid "Settings"
msgstr ""

#: includes/class-eitagcr-settings.php:381
msgid "No cycle"
msgstr ""

#: includes/class-eitagcr-settings.php:396
msgid "General"
msgstr ""

#: includes/class-eitagcr-settings.php:397
msgid "General options for the EITA GCR plugin."
msgstr ""

#: includes/class-eitagcr-settings.php:401
msgid "Force cycle opening"
msgstr ""

#: includes/class-eitagcr-settings.php:402
msgid "Set this option to define if the cycle is open or closed overwriting the cycle parameters."
msgstr ""

#: includes/class-eitagcr-settings.php:407
msgid "The buying cycle is open?"
msgstr ""

#: includes/class-eitagcr-settings.php:408
msgid "Define if the buying cycle is open. This option only takes effect if you set Yes at the option above."
msgstr ""

#: includes/class-eitagcr-settings.php:411
#: includes/class-eitagcr-settings.php:733
msgid "Open"
msgstr ""

#: includes/class-eitagcr-settings.php:412
#: includes/class-eitagcr-settings.php:735
msgid "Closed"
msgstr ""

#: includes/class-eitagcr-settings.php:418
msgid "Current cycle"
msgstr ""

#: includes/class-eitagcr-settings.php:419
msgid "Select the current open cycle."
msgstr ""

#: includes/class-eitagcr-settings.php:421
msgid "Create new cycle"
msgstr ""

#: includes/class-eitagcr-settings.php:427
msgid "Which taxonomy represents suppliers?"
msgstr ""

#: includes/class-eitagcr-settings.php:428
msgid "Choose the among your product taxonomies which one represents the suppliers. This is used to generate supplier related reports."
msgstr ""

#: includes/class-eitagcr-settings.php:434
msgid "Home page when cycle is open"
msgstr ""

#: includes/class-eitagcr-settings.php:435
msgid "Choose the page used for home when cycle is open."
msgstr ""

#: includes/class-eitagcr-settings.php:441
msgid "Home page when cycle is closed"
msgstr ""

#: includes/class-eitagcr-settings.php:442
msgid "Choose the page used for home when cycle is closed."
msgstr ""

#: includes/class-eitagcr-settings.php:448
msgid "Enable delivery place feature"
msgstr ""

#: includes/class-eitagcr-settings.php:449
msgid "Mark here to be able to set different delivery places for each order."
msgstr ""

#: includes/class-eitagcr-settings.php:454
msgid "Warning time threshold (minutes)"
msgstr ""

#: includes/class-eitagcr-settings.php:455
msgid "Set the amount of time to show a warning before the cycle closes."
msgstr ""

#: includes/class-eitagcr-settings.php:463
#: includes/class-eitagcr.php:144
msgid "Cycles"
msgstr ""

#: includes/class-eitagcr-settings.php:464
msgid "Cycles configurations"
msgstr ""

#: includes/class-eitagcr-settings.php:468
msgid "Cycle to configure"
msgstr ""

#: includes/class-eitagcr-settings.php:469
msgid "Select the cycle to edit and see details"
msgstr ""

#: includes/class-eitagcr-settings.php:475
msgid "Cycle name"
msgstr ""

#: includes/class-eitagcr-settings.php:477
msgid "Cycle 3/2020"
msgstr ""

#: includes/class-eitagcr-settings.php:482
msgid "Opening date and time"
msgstr ""

#: includes/class-eitagcr-settings.php:484
#: includes/class-eitagcr-settings.php:491
msgid "aaaa-mm-dd hh:mm:ss"
msgstr ""

#: includes/class-eitagcr-settings.php:489
msgid "Closing date and time"
msgstr ""

#: includes/class-eitagcr-settings.php:500
msgid "Cycle delivery places"
msgstr ""

#: includes/class-eitagcr-settings.php:501
msgid "Uncheck delivery place to disable it for this cycle"
msgstr ""

#: includes/class-eitagcr-settings.php:509
msgid "Report Orders/Customers"
msgstr ""

#: includes/class-eitagcr-settings.php:510
msgid "This report shows the details of each order per customer"
msgstr ""

#: includes/class-eitagcr-settings.php:515
#: includes/class-eitagcr-settings.php:545
#: includes/class-eitagcr-settings.php:565
#: includes/class-eitagcr-settings.php:587
#: includes/class-eitagcr-settings.php:606
#: includes/class-eitagcr-settings.php:627
msgid "Select the cycle to see the report."
msgstr ""

#: includes/class-eitagcr-settings.php:521
msgid "Order by"
msgstr ""

#: includes/class-eitagcr-settings.php:522
msgid "Choose how to order your report."
msgstr ""

#: includes/class-eitagcr-settings.php:525
msgid "Date"
msgstr ""

#: includes/class-eitagcr-settings.php:526
msgid "Name"
msgstr ""

#: includes/class-eitagcr-settings.php:531
#: includes/class-eitagcr-settings.php:551
#: includes/class-eitagcr-settings.php:571
#: includes/class-eitagcr-settings.php:593
#: includes/class-eitagcr-settings.php:612
#: includes/class-eitagcr-settings.php:633
msgid "Include taxes"
msgstr ""

#: includes/class-eitagcr-settings.php:532
#: includes/class-eitagcr-settings.php:552
#: includes/class-eitagcr-settings.php:572
#: includes/class-eitagcr-settings.php:594
#: includes/class-eitagcr-settings.php:613
#: includes/class-eitagcr-settings.php:634
msgid "Includes taxes in this report"
msgstr ""

#: includes/class-eitagcr-settings.php:539
msgid "Report Orders/Category"
msgstr ""

#: includes/class-eitagcr-settings.php:540
#: includes/class-eitagcr-settings.php:560
msgid "This report shows the details of each order per category"
msgstr ""

#: includes/class-eitagcr-settings.php:559
msgid "Report Orders/Category (Summary)"
msgstr ""

#: includes/class-eitagcr-settings.php:581
msgid "Report Orders/Delivery Place"
msgstr ""

#: includes/class-eitagcr-settings.php:582
msgid "This report shows the order details of each delivery place"
msgstr ""

#: includes/class-eitagcr-settings.php:600
msgid "Report Category (Summary)/ Delivery Place"
msgstr ""

#: includes/class-eitagcr-settings.php:601
msgid "This report shows the summary for each catgory by delivery place"
msgstr ""

#: includes/class-eitagcr-settings.php:621
msgid "Report Category (Summary)/ Payment Method"
msgstr ""

#: includes/class-eitagcr-settings.php:622
msgid "This report shows the summary for each catgory by payment method"
msgstr ""

#: includes/class-eitagcr-settings.php:730
msgid "Cycle is "
msgstr ""

#: includes/class-eitagcr-settings.php:790
msgid "Save Settings"
msgstr ""

#: includes/class-eitagcr-settings.php:827
msgid "Generate Report"
msgstr ""

#: includes/class-eitagcr-settings.php:909
#: includes/class-eitagcr-settings.php:988
#: includes/class-eitagcr-settings.php:1100
#: includes/class-eitagcr-settings.php:1182
#: includes/class-eitagcr-settings.php:1292
msgid "Qty"
msgstr ""

#: includes/class-eitagcr-settings.php:910
#: includes/class-eitagcr-settings.php:989
#: includes/class-eitagcr-settings.php:1101
#: includes/class-eitagcr-settings.php:1183
#: includes/class-eitagcr-settings.php:1293
msgid "Product"
msgstr ""

#: includes/class-eitagcr-settings.php:911
#: includes/class-eitagcr-settings.php:991
#: includes/class-eitagcr-settings.php:1102
#: includes/class-eitagcr-settings.php:1185
#: includes/class-eitagcr-settings.php:1294
msgid "Unit price"
msgstr ""

#: includes/class-eitagcr-settings.php:912
#: includes/class-eitagcr-settings.php:992
#: includes/class-eitagcr-settings.php:1103
#: includes/class-eitagcr-settings.php:1186
#: includes/class-eitagcr-settings.php:1295
msgid "Line price"
msgstr ""

#: includes/class-eitagcr-settings.php:915
#: includes/class-eitagcr-settings.php:997
#: includes/class-eitagcr-settings.php:1107
#: includes/class-eitagcr-settings.php:1190
#: includes/class-eitagcr-settings.php:1299
#: includes/class-eitagcr-settings.php:1400
msgid "With tax"
msgstr ""

#: includes/class-eitagcr-settings.php:953
#: includes/class-eitagcr-settings.php:1051
#: includes/class-eitagcr-settings.php:1144
#: includes/class-eitagcr-settings.php:1360
#: includes/class-eitagcr-settings.php:1461
msgid "Total"
msgstr ""

#: includes/class-eitagcr-settings.php:959
#: includes/class-eitagcr-settings.php:1057
#: includes/class-eitagcr-settings.php:1149
#: includes/class-eitagcr-settings.php:1262
#: includes/class-eitagcr-settings.php:1365
msgid "Total with taxes"
msgstr ""

#: includes/class-eitagcr-settings.php:990
#: includes/class-eitagcr-settings.php:1395
msgid "Order"
msgstr ""

#: includes/class-eitagcr-settings.php:1184
msgid "Category"
msgstr ""

#: includes/class-eitagcr-settings.php:1396
msgid "Order Price"
msgstr ""

#: includes/class-eitagcr-settings.php:1476
msgid "Payment Method Total"
msgstr ""

#: includes/class-eitagcr-settings.php:1482
msgid "Payment Method Total (with tax)"
msgstr ""

#: includes/class-eitagcr-settings.php:1492
msgid "Total cycle"
msgstr ""

#: includes/class-eitagcr.php:150
msgid "Delivery places"
msgstr ""

#: includes/class-eitagcr.php:150
msgid "Delivery place"
msgstr ""

#: includes/class-eitagcr.php:291
msgid "Select the delivery place"
msgstr ""

#: includes/lib/class-eitagcr-admin-api.php:170
msgid "Upload an image"
msgstr ""

#: includes/lib/class-eitagcr-admin-api.php:170
msgid "Use image"
msgstr ""

#: includes/lib/class-eitagcr-admin-api.php:170
msgid "Upload new image"
msgstr ""

#: includes/lib/class-eitagcr-admin-api.php:171
msgid "Remove image"
msgstr ""

#: includes/lib/class-eitagcr-post-type.php:103
msgid "Add New"
msgstr ""

#: includes/lib/class-eitagcr-post-type.php:104
#: includes/lib/class-eitagcr-taxonomy.php:106
msgid "Add New %s"
msgstr ""

#: includes/lib/class-eitagcr-post-type.php:105
#: includes/lib/class-eitagcr-taxonomy.php:103
msgid "Edit %s"
msgstr ""

#: includes/lib/class-eitagcr-post-type.php:106
msgid "New %s"
msgstr ""

#: includes/lib/class-eitagcr-post-type.php:108
#: includes/lib/class-eitagcr-taxonomy.php:104
msgid "View %s"
msgstr ""

#: includes/lib/class-eitagcr-post-type.php:109
#: includes/lib/class-eitagcr-taxonomy.php:110
msgid "Search %s"
msgstr ""

#: includes/lib/class-eitagcr-post-type.php:110
msgid "No %s Found"
msgstr ""

#: includes/lib/class-eitagcr-post-type.php:111
msgid "No %s Found In Trash"
msgstr ""

#: includes/lib/class-eitagcr-post-type.php:156
msgid "%1$s updated. %2$sView %3$s%4$s."
msgstr ""

#: includes/lib/class-eitagcr-post-type.php:157
msgid "Custom field updated."
msgstr ""

#: includes/lib/class-eitagcr-post-type.php:158
msgid "Custom field deleted."
msgstr ""

#: includes/lib/class-eitagcr-post-type.php:159
msgid "%1$s updated."
msgstr ""

#: includes/lib/class-eitagcr-post-type.php:160
msgid "%1$s restored to revision from %2$s."
msgstr ""

#: includes/lib/class-eitagcr-post-type.php:161
msgid "%1$s published. %2$sView %3$s%4s."
msgstr ""

#: includes/lib/class-eitagcr-post-type.php:162
msgid "%1$s saved."
msgstr ""

#: includes/lib/class-eitagcr-post-type.php:163
msgid "%1$s submitted. %2$sPreview post%3$s%4$s."
msgstr ""

#: includes/lib/class-eitagcr-post-type.php:164
msgid "%1$s scheduled for: %2$s. %3$sPreview %4$s%5$s."
msgstr ""

#: includes/lib/class-eitagcr-post-type.php:164
msgid "M j, Y @ G:i"
msgstr ""

#: includes/lib/class-eitagcr-post-type.php:165
msgid "%1$s draft updated. %2$sPreview %3$s%4$s."
msgstr ""

#: includes/lib/class-eitagcr-post-type.php:183
msgid "%1$s %2$s updated."
msgid_plural "%1$s %3$s updated."
msgstr[0] ""
msgstr[1] ""

#: includes/lib/class-eitagcr-post-type.php:184
msgid "%1$s %2$s not updated, somebody is editing it."
msgid_plural "%1$s %3$s not updated, somebody is editing them."
msgstr[0] ""
msgstr[1] ""

#: includes/lib/class-eitagcr-post-type.php:185
msgid "%1$s %2$s permanently deleted."
msgid_plural "%1$s %3$s permanently deleted."
msgstr[0] ""
msgstr[1] ""

#: includes/lib/class-eitagcr-post-type.php:186
msgid "%1$s %2$s moved to the Trash."
msgid_plural "%1$s %3$s moved to the Trash."
msgstr[0] ""
msgstr[1] ""

#: includes/lib/class-eitagcr-post-type.php:187
msgid "%1$s %2$s restored from the Trash."
msgid_plural "%1$s %3$s restored from the Trash."
msgstr[0] ""
msgstr[1] ""

#: includes/lib/class-eitagcr-taxonomy.php:102
msgid "All %s"
msgstr ""

#: includes/lib/class-eitagcr-taxonomy.php:105
msgid "Update %s"
msgstr ""

#: includes/lib/class-eitagcr-taxonomy.php:107
msgid "New %s Name"
msgstr ""

#: includes/lib/class-eitagcr-taxonomy.php:108
msgid "Parent %s"
msgstr ""

#: includes/lib/class-eitagcr-taxonomy.php:109
msgid "Parent %s:"
msgstr ""

#: includes/lib/class-eitagcr-taxonomy.php:111
msgid "Popular %s"
msgstr ""

#: includes/lib/class-eitagcr-taxonomy.php:112
msgid "Separate %s with commas"
msgstr ""

#: includes/lib/class-eitagcr-taxonomy.php:113
msgid "Add or remove %s"
msgstr ""

#: includes/lib/class-eitagcr-taxonomy.php:114
msgid "Choose from the most used %s"
msgstr ""

#: includes/lib/class-eitagcr-taxonomy.php:115
msgid "No %s found"
msgstr ""
