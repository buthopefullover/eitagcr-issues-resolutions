<?php
/**
 * Plugin Name: EITA GCR
 * Version: 0.0.1
 * Plugin URI: https://gitlab.com/eita/eitagcr
 * Description: This plugin offers some enhancements to the Woocommerce e-commerce platform to make it fit for using in Responsible Consumer Groups (GCR). It assumes GCRs are composed by several consumers and several suppliers, and the there is a buying cycle.
 * Author: Cooperativa EITA
 * Author URI: https://eita.coop.br
 * Requires at least: 5.7
 * Tested up to: 5.7
 *
 * Text Domain: eitagcr
 * Domain Path: /lang/
 *
 * @package WordPress
 * @author Cooperativa EITA
 * @since 0.0.1
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Load plugin class files.
require_once 'includes/class-eitagcr.php';
require_once 'includes/class-eitagcr-settings.php';

// Load plugin libraries.
require_once 'includes/lib/class-eitagcr-admin-api.php';
require_once 'includes/lib/class-eitagcr-post-type.php';
require_once 'includes/lib/class-eitagcr-taxonomy.php';

require_once 'vendor/autoload.php';

/**
 * Returns the main instance of EitaGCR to prevent the need to use globals.
 *
 * @since  1.0.0
 * @return object EitaGCR
 */
function eitagcr() {
	$instance = EitaGCR::instance( __FILE__, '1.0.0' );

	if ( is_null( $instance->settings ) ) {
		$instance->settings = EitaGCR_Settings::instance( $instance );
	}

	return $instance;
}

eitagcr();
